#include "struct.h"

//the various discussions this server is keeping track of
//its needed to keep track of which clients are part of which discs
disc_node * discs;

// the vars we need to multicast send/rcv 

char    User[MAX_LINE];
char    Spread_name[MAX_LINE];
char    Private_group[MAX_GROUP_NAME];
char    Server_Private_group[MAX_GROUP_NAME];
char    group[MAX_GROUP_NAME];  //our group name
mailbox Mbox;

int		ret;
int		service_type = 0, num_groups;
char	ret_groups[MAX_CLIENTS][MAX_GROUP_NAME];
char	sender[MAX_GROUP_NAME];
char	mess[MAX_BYTES] ;
char	recv_mess[MAX_BYTES] ;
int16	dummy_mess_type;
int		dummy_endian_mismatch;

unsigned int 	my_id;
char			my_name[MAX_NAME + 1];
char 			my_memb_name[MAX_NAME];

int		vectors[NUM_SERVERS + 1][NUM_SERVERS + 1];
char	mappings[NUM_SERVERS + 1][MAX_GROUP_NAME];
short	current_partition_members[NUM_SERVERS + 1];

short	reconciliation[NUM_SERVERS + 1];
int		vectors_left = VALUE_NOT_SET;
int		max_values[NUM_SERVERS + 1];

//TODO: Need to figure out how the gc_vector will be handled, i.e. initialized/stored on disk or not
int		gc_vector[NUM_SERVERS + 1];

//An optimization might be to have an array of new_updates lists
//so that we can store new_updates per server
operation_node*	new_updates = NULL;
operation_node*	new_updates_tail = NULL;

queue_node * qhead = NULL;
int msgs_sent = 0;
int syncs_left = VALUE_NOT_SET;
int sync_sent = 0;

int 	connected = 0;
int 	counter = 0;
int 	object_counter = 0;
int 	op_index_counter = 0;

int		ops_logged = 0;

//TODO: These filenames need to be unique for every server
char recovery_file[MAX_NAME];
char counters_file[MAX_NAME];

int check_ret(int );

void send_to(char*, int, char*, int16);

int dequeue();

void enqueue(char *, int, char *, int16);

void read_op();

object * handle_server_update(operation*, int16);

object * process_update(operation*, short);

void process_new_updates(int);

void send_to_clients(object *, operation *op);

void update_vector(operation*);

void write_matrix();

void handle_connect(char *);

void handle_join(char *, char *);

void remove_client(char *);

void remove_partitioned_clients();

object * apply_append(operation *, int, object *, int, short);

object * apply_delete_edit(operation *, int, object *, int, short);

void apply_recover_op(operation *);

void begin_transaction(operation *);

void end_transaction();

void log_counters();

void handle_print(char *);

int update_partition_logs(operation *, int, int);

void handle_partition_change(int);

void handle_sync(char*);

void handle_sync_ack();

void send_vector();

void recv_vector(char *sender, int vector[NUM_SERVERS + 1]);

void send_matrix(short);

void recv_matrix(int vector[NUM_SERVERS + 1][NUM_SERVERS + 1]);

void perform_gc();

void gc_object(operation *);

void recover();

int main(int argc, char **argv)
{
	struct timeval;
	strcpy(my_name,"Server");
	my_id = atoi(argv[1]);
	strcat(my_name, argv[1]);
	sprintf(my_memb_name, "ServerMemb%d", my_id);

	sprintf(recovery_file, "%irecovery.log", my_id);
	sprintf(counters_file, "%icounters.log", my_id);

	sprintf( User, "projectkkServer" );
	sprintf( Private_group, "projectkk" );
   	sprintf( group, "kanovkhan" );
	sprintf( Spread_name, "%i@localhost", PORT);

	printf("server: connecting to %s\n", Spread_name );
	ret = SP_connect( Spread_name, User, 0, 1, &Mbox, Private_group ) ;
	check_ret(ret);

	debug(1,1,1,"In main: My priv group is %s\n", Private_group);

	//check if we need to recover...
	recover();
	
	//Initialize discs list
	discs = (disc_node *)malloc(sizeof(disc_node));
	discs->next = NULL;
	strcpy(discs->disc.name,"");
	discs->disc.name_list = NULL;

	//join my own group
	SP_join(Mbox, my_name);
	debug(0, 0, 1, "Joined group %s!!\n", my_name);
	SP_join(Mbox, my_memb_name);
	debug(0, 0, 1, "Joined group %s!!\n", my_memb_name);
	SP_join(Mbox, "Control");
	debug(0, 0, 1, "Joined group %s!!\n", "Control");

	do{
		read_op();
		

		//TODO: flow control works but two things left in it:
		//	1. only increment msgs_sent if the msg is being sent to the ctrl grp. this should only
		//	   cause more frequent syncing.
		//	2. the case where a partition change occurs after the SYNC_THRESHOLD is reached
		//	   but before all the SYNC_ACKS are recvd back is not handled. should be trivial..

		//check if its time to sync	
		if(!sync_sent && msgs_sent >= SYNC_THRESHOLD)
		{
			int i;
			syncs_left = 0;
			for(i = 1; i <= NUM_SERVERS; i++)
			{
				if(i == my_id) continue;
				if(current_partition_members[i] == 1) syncs_left++;
			}
	
			ret = SP_multicast( Mbox, AGREED_MESS, "Control", SYNC, 0, NULL );
			if(!check_ret(ret))
			{
				printf("Connection to group lost while sending SYNC.\n");
				return 0 ;
			}
			sync_sent = 1;
		}
	
		//deq if theres something to deq and flow ctrl hasnt kicked in	
		while(qhead && msgs_sent < SYNC_THRESHOLD)
			dequeue();
	
		if (ops_logged > GC_THRESHOLD)
		{
			debug(1, 1, 1, "I am %d and i am initiating gc\n", my_id);
			perform_gc();
			send_matrix(1);
			ops_logged = 0;
		}
	}
	while(1);
	return 0;
}

int check_ret(int ret)
{
	if(ret == GROUPS_TOO_SHORT || ret == BUFFER_TOO_SHORT)
	{
		debug(0, 0, 1, "group/buffer too short error!!\n");
		exit(1);
	}
	else if(ret == CONNECTION_CLOSED)
	{
		debug(0, 0, 1, "Connection closed!!\n");
		printf("Connection closed!!\n");
		exit(0);
	}
	else if( ret < 0 )
	{
		SP_error( ret );
		exit(1);
	}

	return 1;
}


void send_to(char * to, int len, char * msg, int16 mess_type)
{
	debug(1, 1, 1, "send_to params:\n");
	debug(1, 1, 1, "\tto: %s\n", to);
	//debug(1, 1, 1, "\tmsg: %s\n", msg + sizeof(action));
	debug(1, 1, 1, "\tmess_type: %d\n", mess_type);
	debug(1, 1, 1, "\tlen: %d\n", len);
	enqueue(to, len, msg, mess_type);
}

void enqueue(char * to, int len, char * msg, int16 mess_type)
{
	debug(1, 1, 1, "Enqing params:\n");
	debug(1, 1, 1, "\tto: %s\n", to);
	//debug(1, 1, 1, "\tmsg: %s\n", msg + sizeof(action));
	debug(1, 1, 1, "\tmess_type: %d\n", mess_type);
	debug(1, 1, 1, "\tlen: %d\n", len);

	queue_node * new = (queue_node*)malloc(sizeof(queue_node));
	memset((char*)new, '\0', sizeof(queue_node));
	new->msg = (char*)malloc(len);
	strcpy(new->recipient, to);
	new->len = len;
	memcpy(new->msg, msg, len);
	new->tag = mess_type;
	
	debug(1, 1, 1, "Enqing:\n");
	debug(1, 1, 1, "\tnew->recipient: %s\n", new->recipient);
	//debug(1, 1, 1, "\tnew->msg: %s\n", new->msg + sizeof(action));
	debug(1, 1, 1, "\tnew->tag: %d\n", new->tag);
	debug(1, 1, 1, "\tnew->len: %d\n", new->len);
	new->next = qhead;
	qhead = new;
}

int dequeue()
{
	debug(1, 1, 1, "dequeue: start..\n");
	queue_node * iter, *prev = NULL;

	//if empty, then nothing to do
	if(!qhead)
	{
		debug(1, 1, 1, "qhead was null. returning.\n");
		return 1;
	}

	iter = qhead;
	while(iter->next)
	{
		prev = iter;
		iter = iter->next;
	}


	debug(1, 1, 1, "Deqing:\n");
	debug(1, 1, 1, "\titer->recipient: %s\n", iter->recipient);
	//debug(1, 1, 1, "\titer->msg: %s\n", iter->msg + sizeof(action));
	debug(1, 1, 1, "\titer->tag: %d\n", iter->tag);
	debug(1, 1, 1, "\titer->len: %d\n", iter->len);

	ret = SP_multicast( Mbox, AGREED_MESS, iter->recipient, iter->tag, iter->len, iter->msg );
	if(!check_ret(ret))
	{
		printf("Connection to group %s lost.\n", iter->recipient);
		return 0 ;
	}

	if(iter == qhead)
	{
		debug(1, 1, 1, "\tDeqing the only node\n");
		qhead = NULL;
	}
	else
		prev->next = NULL;
	free(iter->msg);
	free(iter);

	msgs_sent++;

	return 1;

}

void read_op()
{
	operation op;
	object * obj = NULL;
	service_type = 0;
    ret = SP_receive( Mbox, &service_type, sender, MAX_CLIENTS, &num_groups, ret_groups,
                                  &dummy_mess_type, &dummy_endian_mismatch, sizeof(recv_mess), recv_mess );
	check_ret(ret);

	if(strcmp(sender, Private_group) == 0)
		return;

	debug(1, 1, 1, "\n***************************\nRead op:\n");
	
	if(Is_regular_mess(service_type))
	{
		if(strcmp(ret_groups[0], Private_group) == 0)
		{
			//unicast from client
			//shouldnt get a connect request here!!

			debug(1, 1, 1, "\t unicast msg recvd.\n");
			if(dummy_mess_type == SYNC_ACK)
			{
				debug(1, 1, 1, "\t msg is a SYNC_ACK reply from a server.\n");
				handle_sync_ack();
			}
			else
			{
				debug(1, 1, 1, "\t msg is from a client.\n");
				memcpy(&op, recv_mess, sizeof(operation));
				dump_op(&op);
				
				assert(op.act != CONNECT);
				if(op.act == JOIN)
				{
					handle_join(sender, op.disc_name);
				}
				else if(op.act == PRINT)
					handle_print(sender);
				else if (op.act == EDIT || op.act == DELETE || op.act == APPEND){
					obj = process_update(&op, 0);
				}
				else
					debug(1, 1, 1, "Unknown operation from client\n");

				if(obj)
				{
					dump_obj(obj);

					//Note that it is OK to leave this out of the transaction
					//If we crash before sending it we will send it when we reappear and reconcile
					//our clients will also get it if they connect after we recover
					send_to("Control", sizeof(operation), (char*)&op, OPERATION);
					send_to_clients(obj, &op);
				}
			}
		}
		else if(strcmp(ret_groups[0], my_name) == 0)
		{
			//initial reqd from client

			debug(1, 1, 1, "\t client connect operation\n");
			memcpy(&op, recv_mess, sizeof(operation));
			dump_op(&op);

			assert(op.act == CONNECT);
			handle_connect(sender);
		}
		else if(strcmp(ret_groups[0], "Control") == 0)
		{
			//from the server group

			debug(1, 1, 1, "\t message from the server Control group\n");
			
			if (dummy_mess_type == VECTOR){
				debug(1, 1, 1, "\t the message contains a vector\n");
				//TODO: do we need to check anything here?
				recv_vector(sender, (int *)recv_mess);
			}
			else if (dummy_mess_type == MATRIX){
				debug(1, 1, 1, "\t The message contains a matrix!\n");
				//TODO: do we need to check anything here?

				//recv_matrix expects a two dimensional int array
				//parsing the recved message as a void pointer should work
				assert(ret == (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int));
				recv_matrix((void *)recv_mess);

				perform_gc();
			}
			else if (dummy_mess_type == PROPOSE_GC){
				debug(1, 1, 1, "\t The message contains a propose_gc matrix! Somebody has decided to do gc!\n");
				//TODO: do we need to check anything here?

				//recv_matrix expects a two dimensional int array
				//parsing the recved message as a void pointer should work
				assert(ret == (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int));
				recv_matrix((void *)recv_mess);

				//since this is a propose gc message we need to send our matrix
				send_matrix(0);
				ops_logged = 0;
				perform_gc();

				//each server in the partition will perform gc once they start receiving the matrixes
				//therefore we should reset the gc timer here
			}
			else if (dummy_mess_type == OPERATION || dummy_mess_type == RECONCILIATION_OP){
				debug(1, 1, 1, "\t the message contains an operation\n");
				memcpy(&op, recv_mess, sizeof(operation));
				dump_op(&op);

				if(op.act == EDIT || op.act == DELETE || op.act == APPEND || op.act == PURGED){
					obj = handle_server_update(&op, dummy_mess_type);
				}
				else
					debug(1, 1, 1, "Unknown operation from \"Control\" group\n");

				if(obj)
				{
					//Note that it is OK to leave this out of the transaction
					//If we crash before sending it we will send it when we reappear and reconcile
					//our clients will also get it if they connect after we recover
					send_to_clients(obj, &op);
				}
			}
			else if (dummy_mess_type == SYNC){
				debug(1, 1, 1, "\t the message contains a SYNC!!!\n");
				handle_sync(sender);				
			}
			else
				debug(1, 1, 1, "Unknown msg type %d from \"Control\" group\n", dummy_mess_type);
		}
		else
			debug(1, 1, 1, "Msg from unknown entity arrived to group %s\n", ret_groups[0]);
	}
	else if(Is_membership_mess(service_type))
	{
		//this msg should concern our control grp
		//Kalin: not necessarily :)
		//TODO: We need to detect clients that have left and remove them from our structure
		//assert(strcmp("Control", sender) == 0);
		//int i, id = 0;

		if(Is_reg_memb_mess(service_type))
		{
			if(Is_caused_network_mess(service_type))
			{
				debug(1, 1, 1, "\t Message from Spread caused by network\n");
				if (strcmp("Control", sender) == 0)
					handle_partition_change(num_groups);

				if (strcmp(my_memb_name, sender) == 0){
					debug(1,1,1, "\t I cannot talk to the client anymore, should I remove him from my discs?\n");
					remove_partitioned_clients();
				}
			}
			else if(Is_caused_disconnect_mess(service_type))
			{
				debug(1, 1, 1, "\t Message from Spread caused by disconnect\n");
				//someone has crashed
				
				if (strcmp("Control", sender) == 0)
					handle_partition_change(num_groups);

				if (strcmp(my_memb_name, sender) == 0){
					debug(1,1,1, "\t The client must have disconnected, need to remove him from my discs\n");
					membership_info memb;
					SP_get_memb_info(recv_mess, service_type, &memb);
					remove_client(memb.changed_member);
				}
			}
			else if(Is_caused_join_mess(service_type))
			{
				debug(1, 1, 1, "\t Message from Spread caused by join\n");
				//a server has joined our control grp


				if (strcmp("Control", sender) == 0)
					handle_partition_change(num_groups);

				if (strcmp(my_memb_name, sender) == 0)
					debug(1,1,1, "\t Someone joined my memb group, I can now keep track of him\n");
			}
			else if(Is_caused_leave_mess(service_type))
			{
				//a server has left our control grp
				//servers shouldnt voluntarily leave the group!

				if (strcmp("Control", sender) == 0)
					assert(0);

				if (strcmp(my_memb_name, sender) == 0){
					debug(1,1,1, "\t A client left my group, probably to connect to another server, removing him...\n");
					membership_info memb;
					SP_get_memb_info(recv_mess, service_type, &memb);
					remove_client(memb.changed_member);
				}
			}
		}	
		else if(Is_caused_leave_mess(service_type))
		{
			//why did we leave the group???
			assert(0);
		}
	}
}

object * handle_server_update(operation *op, int16 dummy_mess_type){
	object* obj;
	obj = NULL;
	int server_index = op->timestamp.id;
	//we need to check if we are in the process of reconciling updates generated by the server that timestamped this op
	if (reconciliation[server_index])
	{
		debug(1, 1, 1, "handle_server_update: op recvd while being reconciled\n");
		//if this is the case then we need to check if this is a reconciliation op
		if (dummy_mess_type == RECONCILIATION_OP){
			debug(1, 1, 1, "handle_server_update: It is a reconciliation op\n");
			if (op->act == PURGED){
				//if it is a PURGED operation (we don't need to process it) it must be the last operation for this server
				debug(1, 1, 1, "handle_server_update: recvd PURGED operation => last reconciliation op recvd\n");
				assert(max_values[server_index] == op->op_index);

				update_vector(op);
				reconciliation[server_index] = 0; //this reconciliation is done
				//need to process all the new updates from the server that were buffered until now
				process_new_updates(server_index);
				//we return NULL since we have sent all the objects/operations to the clients above
				return NULL;
			}

			//if this is a reconciliation operation we can go ahead and process it and update our vector
			obj = process_update(op, 1);

			//check if this is the last reconciliation operation that is expected
			//and if so update our vector to the value stored in the new_updates vector
			if (op->op_index == max_values[server_index]){
				if(obj)
					send_to_clients(obj, op);

				debug(1, 1, 1, "handle_server_update: Last reconciliation op recvd\n");
				reconciliation[server_index] = 0; //this reconciliation is done
				//need to process all the new updates from the server that were buffered until now
				process_new_updates(server_index);
				//we return NULL since we have sent all the objects/operations to the clients above
				return NULL;
			}

		}
		else{
			debug(1, 1, 1, "handle_server_update: it is a client op, buffering...\n");
			//otherwise we buffer the operation to be processed after we are done reconciling
			//the operation needs to be put at the end of the new_updates list
			operation_node *new_op = (operation_node *)malloc(sizeof(operation_node));
			new_op->next = NULL;
			memcpy(&new_op->op, op, sizeof(operation));
			if (new_updates_tail){
				new_updates_tail->next = new_op;
				new_updates_tail = new_op;
			}
			else{
				new_updates_tail = new_op;
				new_updates = new_op;
			}
		}
	}
	else{
		debug(1, 1, 1, "handle_server_update: op recvd while NOT being reconciled\n");
		//we are not being reconciled with this server's updates
		if (dummy_mess_type == RECONCILIATION_OP)
			debug(1, 1, 1, "Reconciliation operation ignored! I don't need it!\n");
		else
			//we can process new updates in the normal way, i.e. update all vector and all
			obj = process_update(op, 1);
	}
	debug(1, 1, 1, "handle_server_update: returning obj value as %d\n", obj == NULL? 0: 1);
	return obj;
}

object * process_update(operation *op, short server_op){
	int index = VALUE_NOT_SET;
	object *disc = NULL, *ret_obj = NULL;
	int num_objects = VALUE_NOT_SET;
	short exists = 0;
	short proceed = 0;

	debug(1, 1, 1, "process_update: start..\n");
	dump_op(op);
	debug(1, 1, 1, "process_update: server_op is %d\n", server_op);

	//update the LTS counter if necessary
	if (server_op){
		if (op->timestamp.counter > counter){
			debug(1, 1, 1, "process_update: Its a server op. Updating our counter to %d\n", op->timestamp.counter);
			counter = op->timestamp.counter;
			//at this point we have updated our counter, but we may not proceed below,
			//therefore we should probably just write to the counters file at this point whatever happens
			//we want to have are most current counter on disk
			log_counters();
		}
	}
	else{
		//need to stamp
		op->timestamp.id = my_id;
		counter++;
		op->timestamp.counter = counter;

		op_index_counter++;
		op->op_index = op_index_counter;

		if (op->act == APPEND){
	    	copy_LTS(&op->append_lts, &op->timestamp);

			//Create a new object id for the object
			//Store it in the "object id" file
			object_counter++;
			op->obj_id = object_counter;
			op->obj_id = op->obj_id << 3;
			op->obj_id += my_id;
		}
		//if this is a client op we want to always proceed
		//since we will always proceed we don't need to log the counters
		proceed = 1;
		debug(1, 1, 1, "process_update: Its a client op. LTS counter is %d obj counter %d op_index %d \n", counter, object_counter, op_index_counter);
	}
	
	debug(1, 1, 1, "process_update: Dumping op after stamping\n");
	dump_op(op);

	if (op->act != APPEND || server_op){
		//TODO: handle case where disc does not exist
		char disc_filename[MAX_NAME + 1];
		sprintf(disc_filename, "%i_", my_id);
		strcat(disc_filename,op->disc_name);
		FILE *fd = fopen(disc_filename,"r");

		if(fd){
			fseek(fd, 0, SEEK_END);
			int file_size = ftell(fd);
			debug(1, 1, 1, "process_update: Not a client append.\n");
			assert(file_size % sizeof(object) == 0);
			num_objects = file_size/sizeof(object);
			disc = (object*)malloc(file_size);
			fseek(fd, 0, SEEK_SET);
			fread(disc, 1, file_size, fd);
			assert(file_size % sizeof(object) == 0);
			debug(1, 1, 1, "process_update: This is the disc we have on file.\n");
			dump_disc(num_objects, disc);
		}
		else{
			num_objects = 0;
		}

		int i;
		for (i = 0; i < num_objects; i++){
			debug(1, 1, 1, "process_update: is obj %d equal to the op->obj_id %d?\n", disc[i].id, op->obj_id);
			if (disc[i].id == op->obj_id){
				debug(1, 1, 1, "process_update: found the obj this op applies to\n");
				index = i;
				exists = 1;
				proceed = 1;
				if (server_op){
					if (disc[i].last_op.act == DELETE)
						//DELETE always wins!
						proceed = 0;
					else if (op->act == DELETE){
						//DELETE always wins!
						proceed = 1;
					}
					else if (compare_LTS(&op->timestamp, &disc[i].last_op.timestamp) != FIRST_GREATER){
						//this operation should NOT be applied
						proceed = 0;
					}
				}
				else
				{
					copy_LTS(&op->append_lts, &disc[i].last_op.append_lts);
				}
				break;
			}
		}

		if (!exists){
			debug(1, 1, 1, "process_update: obj doesnt exist\n");
			//handle object not found
			if (server_op){
				//this operation should be applied
				//index needs to be the position where the object should be inserted (should we do this above?)
				if (disc){
					i = num_objects - 1;
					assert(compare_LTS(&op->append_lts, &disc[i].last_op.append_lts) != EQUAL);
					while (compare_LTS(&op->append_lts, &disc[i].last_op.append_lts) == SECOND_GREATER){
						i--;
						if (i == -1)
							break;
						assert(compare_LTS(&op->append_lts, &disc[i].last_op.append_lts) != EQUAL);
					}
					index = i + 1;
					proceed = 1;
				}
				else{
					//we do not have this discussion, this should be the first object in the discussion
					proceed = 1;
					index = 0;
				}
			}
			else{
				//the client has only the objects, discussions, etc. that the server has
				//how can the client have an object that the server does not?
				assert(0);
			}
		}
    }

	ret_obj = NULL;
	if(proceed)
	{
		begin_transaction(op);
		if(op->act == EDIT || op->act == DELETE){
			ret_obj = apply_delete_edit(op, index, disc, num_objects, exists);
		}
		else if (op->act == APPEND){
			ret_obj = apply_append(op, index, disc, num_objects, !server_op);
		}
		//TODO: Need to consider whether we might have to update our vector even if we don't "proceed"
		update_vector(op);
		end_transaction();
	}

	if(disc)
		free(disc);
	return ret_obj;
	
}

void process_new_updates(int server_index){
	operation_node *iter = new_updates;
	object *obj;
	operation_node *del = NULL, *prev = NULL;
	while(iter){
		if (iter->op.timestamp.id == server_index){
			obj = process_update(&iter->op, 1);
			if (obj)
				send_to_clients(obj, &iter->op);

			//are we at the head?
			if(iter == new_updates)
			{
				//is this also the tail?
				if(iter == new_updates_tail )
					new_updates = new_updates_tail = NULL;
				else //there is more than one node, so just inc head
					new_updates = new_updates->next;
			}
			else
			{
				//are we at the last node?
				if(iter == new_updates_tail )
				{
					//adjust tail
					new_updates_tail = prev;
					new_updates_tail->next = NULL;
				}
				else //maintain prev pointer
					prev->next = iter->next;
			}
			//we have identified the node to be deleted, so delete it
			//but inc iter as we need it
			del = iter;
			iter = iter->next;
			free(del);
		}
		else
		{
			//iter does not need to be processed, so just inc pointers
			prev = iter;
			iter = iter->next;
		}
	}
}

void send_to_clients(object *obj, operation *op){
	disc_node * temp_node = discs;
	client_node * temp_client;
	int len = sizeof(action) + sizeof(object);
	char * response = (char *)malloc(len);
	int i;
	for(i = 0; i < len; i++)
		response[i] = '\0';

	memcpy(response, &op->act, sizeof(action));

	debug(1, 1, 1, "send_to_clients: sending action %d\n", op->act);
	//lets prepare the response
	if(op->act == APPEND || op->act == EDIT || op->act == DELETE)
		memcpy(response + sizeof(action), obj, sizeof(object));
	else
	{
		debug(1, 1, 1, "send_to_clients: Something other than append, edit or delete being sent. Exiting..\n");
		exit(0);
	}

	//find the discussion first
	while(temp_node)
	{
		debug(1, 1, 1, "send_to_clients: checking discussion %s\n", temp_node->disc.name);
		temp_client = temp_node->disc.name_list;
		if(strcmp(temp_node->disc.name, op->disc_name) == 0)
		{
			debug(1, 1, 1, "send_to_clients: found discussion %s\n", temp_node->disc.name);
			//send to all clients
			while(temp_client)
			{
				debug(1, 1, 1, "send_to_clients: sending to %s\n", temp_client->client_private_group);
				send_to(temp_client->client_private_group, len, response, 0);
				temp_client = temp_client->next;
			}
			break;
		}
		temp_node = temp_node->next;
	}
	free(response);
	debug(1, 1, 1, "send_to_clients: returning...\n");
}

void update_vector(operation *op){
	//We would also need to  write our vector out
	//TODO: We should probably write out our entire matrix
	if (vectors[my_id][op->timestamp.id] < op->op_index){
		debug(1,1,1,"operation received applied to vector vectors[%i][%i] = [%i]\n", my_id, op->timestamp.id, op->op_index);
		vectors[my_id][op->timestamp.id] = op->op_index;

		write_matrix();
	}
	else{
		debug(1,1,1,"operation received not applied to vector\n");
	}
}

void write_matrix(){
	FILE *fd = fopen(recovery_file, "r+");
	if (!fd){
		debug(1, 1, 1, "update_vector: could not open file %s for writing\n", recovery_file);
		return;
	}

	//write the vector to the recovery file
	//seek over the flag and the temp operation
	fseek(fd, sizeof(int) + sizeof(operation), SEEK_SET);
	fwrite(vectors, 1, (NUM_SERVERS + 1) * (NUM_SERVERS + 1) * sizeof(int), fd);
	fclose(fd);
}

void handle_connect(char * sender)
{
	debug(1,1,1,"handle_connect: My priv group is: %s and strlen is %d\n", Private_group, strlen(Private_group));
	//get the private group of the client and store it in a disc_node
	client_node *temp_client;
	temp_client = (client_node *)malloc(sizeof(client_node));
	strcpy(temp_client->client_private_group, sender);
	temp_client->next = discs->disc.name_list;
	discs->disc.name_list = temp_client;

	char * response = (char*)malloc(sizeof(action) + strlen(Private_group) + 1);
	action act = CONNECT;
	memcpy(response, &act, sizeof(action));
	memcpy(response + sizeof(action), Private_group, strlen(Private_group) + 1);

	debug(1,1,1,"About to return from handle_conn and response is %s and len is %d\n", response + sizeof(action), strlen(Private_group));	

	send_to(discs->disc.name_list->client_private_group, sizeof(action) + strlen(Private_group) + 1, response, 0);
	free(response);
}

void handle_join(char * sender, char *disc_name)
{
	int found_desired_disc = 0;
	int file_size = 0;
	object * objects = NULL;
	discussion disc;
	memset((char*)&disc, '\0', sizeof(discussion));
	strcpy(disc.name,disc_name);

	client_node *temp_client;
	client_node *prev;
	disc_node *temp_disc;
	temp_disc = discs;
	//NOTE: We should always have at least one node in discs - for the connected clients
	assert(temp_disc);

	debug(1, 1, 1, "handle_join: Starting handle_join for discussion %s and sender %s\n", disc_name, sender);

	remove_client(sender);

	while(temp_disc){
		temp_client = temp_disc->disc.name_list;
		prev = NULL;
		
		debug(1, 1, 1, "handle_join:Checking discussion %s\n", temp_disc->disc.name);

		if (strcmp(temp_disc->disc.name, disc_name) == 0){
			found_desired_disc = 1;
			debug(1, 1, 1, "handle_join: Found discussion %s which client %s wanted to join\n", temp_disc->disc.name, sender);
			client_node *new_node = (client_node *)malloc(sizeof(client_node));
			strcpy(new_node->client_private_group, sender);
			new_node->next = temp_disc->disc.name_list;
			temp_disc->disc.name_list = new_node;
			break;
		}
		temp_disc = temp_disc->next;
	}

	//Note that there is a situation where we did not find the desired disc above, but the filename still exists
	//this can happen if we crash and recover, since we will not be recovering the discs list
	char disc_filename[MAX_NAME + 1];
	sprintf(disc_filename, "%i_", my_id);
	strcat(disc_filename, disc_name);
	FILE *fd = fopen(disc_filename, "r");
	if (fd){
		fseek(fd, 0, SEEK_END);
		file_size = ftell(fd);
		debug(1, 1, 1, "handle_join: The discussion file %s exists and has %i objects in it (file_size = %i)\n", disc_name, file_size/sizeof(object), file_size);
		objects = (object *)malloc(file_size);
		fseek(fd, 0, SEEK_SET);
		fread(objects, 1, file_size, fd);
		dump_disc(file_size/sizeof(object), objects);
		fclose(fd);
	}
	else{
		debug(1, 1, 1, "handle_join: The discussion file %s does not exist. Creating it..\n", disc_name);
		char disc_filename[MAX_NAME + 1];
		sprintf(disc_filename, "%i_", my_id);
		strcat(disc_filename, disc_name);
		FILE *fd = fopen(disc_filename, "w");
		fclose(fd);
		file_size = 0;
		objects = NULL;
	}

	if(!found_desired_disc){
		//If above the file existed we must have crashed and recovered,
		//this is why we didn't find this discussion in the discs list,
		//but we do have the file

		disc_node *iter, * temp_node = (disc_node*) malloc(sizeof(disc_node));
		client_node * temp_client = (client_node*) malloc(sizeof(client_node));
		strcpy(temp_client->client_private_group, sender);	
		temp_client->next = NULL;

		iter = discs;
		while(iter->next)
		{
			iter = iter->next;
		}

		iter->next = temp_node;
		strcpy(temp_node->disc.name, disc_name);
		temp_node->disc.name_list = temp_client;
		temp_node->next = NULL;
	}

	disc.num_objs = file_size/sizeof(object);
	disc.head = disc.tail = NULL;
	
	char *join_response = (char *)malloc(file_size + sizeof(action) + sizeof(discussion));
	memset(join_response, '\0', file_size + sizeof(action) + sizeof(discussion));
	 
	action act = JOIN;
	memcpy(join_response, &act, sizeof(action));
	memcpy(join_response + sizeof(action), &disc, sizeof(discussion));
	memcpy(join_response + sizeof(action) + sizeof(discussion), (char *)objects, file_size);
	
	debug(0, 1, 1, "Sending discussion name %s with %d objects\n", disc.name, disc.num_objs);

	send_to(sender, sizeof(action) + sizeof(discussion) + file_size, join_response, 0);
	free(join_response);
	if (objects)
		free(objects);
}

void remove_partitioned_clients()
{
	client_node *temp_client, *next_node;
	client_node *prev;
	disc_node *temp_disc;
	temp_disc = discs;
	int i = 0;
	while(temp_disc){
		temp_client = temp_disc->disc.name_list;
		prev = NULL;

		while(temp_client){
			debug(1, 1, 1, "remove_partitioned_client: Is client %s still with us?\n", temp_client->client_private_group);
			for(i = 0; i < num_groups; i++)
			{
				if (strcmp(temp_client->client_private_group, ret_groups[i]) == 0){
					debug(1, 1, 1, "remove_partitioned_client: Client %s is still with us \n", ret_groups[i]);

					break;

				}
			}
			
			if(i == num_groups)
			{
				//this client is not with is anymore
				//remove him
				
				if (prev == NULL){
					temp_disc->disc.name_list = temp_disc->disc.name_list->next;
				}
				else{
					prev->next = temp_client->next;
				}
				next_node = temp_client->next;
				free(temp_client);
				temp_client = next_node;
				continue;
			}

			prev = temp_client;
			temp_client = temp_client->next;
		}
		temp_disc = temp_disc->next;
	}
}

void remove_client(char *sender){
	client_node *temp_client;
	client_node *prev;
	disc_node *temp_disc;
	temp_disc = discs;
	//NOTE: We should always have at least one node in discs - for the connected clients
	assert(temp_disc);

	while(temp_disc){
		temp_client = temp_disc->disc.name_list;
		prev = NULL;

		while(temp_client){
			debug(1, 1, 1, "remove_client: Is client %s equal to stored client %s\n", sender, temp_client->client_private_group);
			if (strcmp(temp_client->client_private_group, sender) == 0){
				debug(1, 1, 1, "remove_client: Found discussion %s which client %s was previously in\n", temp_disc->disc.name, sender);
				if (prev == NULL){
					temp_disc->disc.name_list = temp_disc->disc.name_list->next;
				}
				else{
					prev->next = temp_client->next;
				}
				free(temp_client);
				return;
			}
			prev = temp_client;
			temp_client = temp_client->next;
		}
		temp_disc = temp_disc->next;
	}
}

object * apply_append(operation *op, int index, object *disc, int num_objects, short client_operation)
{
	FILE *fd;
	object *ob;
	ob = (object*) malloc(sizeof(object));
	memset((char*)ob, '\0', sizeof(object));
	int i;
	for(i = 0; i < MAX_LINE + 1; i++)
		ob->line[i] = '\0';
	strcpy(ob->line, op->new_val);
	ob->visible = 1;
	ob->id = op->obj_id;

	char disc_filename[MAX_NAME + 1];
	sprintf(disc_filename, "%i_", my_id);
	strcat(disc_filename, op->disc_name);

	debug(1, 1, 1, "apply_append: start\n");
	dump_op(op);

	memcpy(&ob->last_op,op,sizeof(operation));

	ob->offset = update_partition_logs(op, VALUE_NOT_SET, VALUE_NOT_SET);
	ob->logfile_id = op->timestamp.id;

	//write out the object in the disc_file at the appropriate offset
	if (!client_operation && disc){
		//if we are passed the discussion we need to insert the object
		dump_disc(num_objects, disc);

		char temp_filename[MAX_NAME + 1];
		sprintf(temp_filename, "%i_tempfile", my_id);
		FILE *temp_file = fopen(temp_filename,"w");

		fwrite(disc, index, sizeof(object), temp_file);
		fwrite(ob, 1, sizeof(object), temp_file);
		fwrite(disc + index, num_objects - index, sizeof(object),  temp_file);
		rename(temp_filename,disc_filename);
		fclose(temp_file);
	}
	else{
		//This is either a client append or the discussion was not found
		//We should just need to put it at the end (create the file if it does not exist)
		fd = fopen(disc_filename, "a");
		fwrite(ob, 1, sizeof(object), fd);
		fclose(fd);
	}
	return ob;
}

object * apply_delete_edit(operation *op, int index, object *disc, int num_objects, short exists)
{
	FILE *fd;
	object * ob = (object*) malloc(sizeof(object));
	memset((char*)ob, '\0', sizeof(object));
	char disc_filename[MAX_NAME + 1];
	sprintf(disc_filename, "%i_", my_id);
	strcat(disc_filename, op->disc_name);

	debug(1, 1, 1, "apply_delete_edit: start..\n");
	dump_op(op);
	dump_disc(num_objects, disc);

	debug(1, 1, 1, "exists is %d and index is %d\n", exists, index);

	if (!exists){
		ob->id = op->obj_id;
		strcpy(ob->line, op->new_val);
		ob->offset = VALUE_NOT_SET;
		ob->visible = 1;
	}
	else{
		//the object exists and therefore the file must exist as well
		fd = fopen(disc_filename,"r+");
		assert(fd);
		//we seek to the appropriate place and read-in the object
		fseek(fd, index*sizeof(object), SEEK_SET);
		fread(ob, 1, sizeof(object), fd);
	}

	if (op->act == DELETE){
		ob->visible = 0;
	}
	else if (op->act == EDIT){
		strcpy(ob->line,op->new_val);
		ob->visible = 1;
	}
	else{
		assert(0);
	}
	memcpy(&ob->last_op,op,sizeof(operation));

	ob->offset = update_partition_logs(op, ob->logfile_id, ob->offset);
	ob->logfile_id = op->timestamp.id;

	//write out the object in the disc_file at the appropriate offset
	if (!exists){
		if (disc){
			//if we are passed the discussion we need to insert the object
			char temp_filename[MAX_NAME + 1];
			sprintf(temp_filename, "%i_tempfile", my_id);
			FILE *temp_file = fopen(temp_filename,"w");

			fwrite(disc, index, sizeof(object), temp_file);
			fwrite(ob, 1, sizeof(object), temp_file);
			fwrite(disc + index, num_objects - index, sizeof(object),  temp_file);
			rename(temp_filename,disc_filename);
			fclose(temp_file);
		}
		else{
			assert(fopen(disc_filename, "r+") == NULL);
			//this discussion must not have been found, i.e. we do not have the file
			//we need to create the file and put the object in it
			fd = fopen(disc_filename, "w");
			fwrite(ob, 1, sizeof(object), fd);
			fclose(fd);
		}
	}
	else{
		fseek(fd, index * sizeof(object), SEEK_SET);
		fwrite(ob, 1, sizeof(object), fd);
		fclose(fd);
	}
	return ob;
}

void apply_recover_op(operation *op)
{
	int index = VALUE_NOT_SET;
	object *disc = NULL;
	int num_objects = 0;
	short exists = 0;
	object *ob = (object*) malloc(sizeof(object));

	char disc_filename[MAX_NAME + 1];
	sprintf(disc_filename, "%i_", my_id);
	strcat(disc_filename, op->disc_name);
    FILE *fd = fopen(disc_filename,"r");
    //check whether this discussion's file exists
    //if not create it
    if (!fd){
    	fd = fopen(disc_filename, "w");
    }
    else{
		fseek(fd, 0, SEEK_END);
		int file_size = ftell(fd);

		assert(file_size % sizeof(object) == 0);
		num_objects = file_size/sizeof(object);
		disc = (object*)malloc(file_size);
		fseek(fd, 0, SEEK_SET);
		fread(disc, 1, file_size, fd);
    }

	int i;
	for (i = 0; i < num_objects; i++){
		if (disc[i].id == op->obj_id){
			memcpy(ob, &disc[i], sizeof(object));
			index = i;
			exists = 1;
			assert(disc[i].last_op.act != DELETE);
			if (compare_LTS(&op->timestamp, &disc[i].last_op.timestamp) == EQUAL){
				//this operation should NOT be applied
				return;
			}
			else{
				assert(compare_LTS(&op->timestamp, &disc[i].last_op.timestamp) == FIRST_GREATER);
			}
			break;
		}
	}

	if (!exists){
		//handle object not found
		//this operation should be applied
		//index needs to be the position where the object should be inserted (should we do this above?)
		ob->id = op->obj_id;
		strcpy(ob->line, op->new_val);
		//TODO: Should we worry about this operation having been written to the partition log already
		//		Seems that the worst that can happen is that we put it there twice
		//TODO: In order to resolve this we could read the last operation in the log and see if it is already there
		ob->offset = update_partition_logs(op, VALUE_NOT_SET, VALUE_NOT_SET);
		ob->logfile_id = op->timestamp.id;

		i = num_objects - 1;
		if (disc){
			assert(compare_LTS(&op->timestamp, &disc[i].last_op.append_lts) != EQUAL);
			while (compare_LTS(&op->timestamp, &disc[i].last_op.append_lts) == SECOND_GREATER){
				i--;
				if (i == -1)
					break;
				assert(compare_LTS(&op->timestamp, &disc[i].last_op.append_lts) != EQUAL);
			}
		}
		index = i + 1;
	}
	else{
		//TODO: Should we worry about this operation having been written to the partition log already
		//		Seems that the worst that can happen is that we put it there twice
		//TODO: In order to resolve this we could read the last operation in the log and see if it is already there
		ob->offset = update_partition_logs(op, ob->logfile_id, ob->offset);
		ob->logfile_id = op->timestamp.id;
	}

	if (op->act == DELETE){
		ob->visible = 0;
	}
	else if (op->act == EDIT || op->act == APPEND){
		strcpy(ob->line,op->new_val);
		ob->visible = 1;
	}
	else{
		assert(0);
	}
	memcpy(&ob->last_op,op,sizeof(operation));

	//write out the object in the disc_file at the appropriate offset
	if (!exists && disc){
		char temp_filename[MAX_NAME + 1];
		sprintf(temp_filename, "%i_tempfile", my_id);
		FILE *temp_file = fopen(temp_filename,"w");

		fwrite(disc, index, sizeof(object), temp_file);
		fwrite(ob, 1, sizeof(object), temp_file);
		fwrite(disc + index, num_objects - index, sizeof(object),  temp_file);
		rename(temp_filename,disc_filename);
		fclose(temp_file);
	}
	else{
		fseek(fd, index * sizeof(object), SEEK_SET);
		fwrite(ob, 1, sizeof(object), fd);
		fclose(fd);
	}
	free(ob);
	if (disc)
		free(disc);
}

void begin_transaction(operation *op){
	//TODO: We should probably write out our entire matrix rather than just the vector,
	//		but writing out just our vector should also work
	//NOTE: Operation should have been stamped in pre-processing with an LTS if it is a client operation
	FILE *fd = fopen(recovery_file, "w");
	if (!fd){
		debug(1, 1, 1, "begin_transaction: could not open file %s for writing\n");
	}
	//write the operation to the temp operation file
	char * transaction = (char *)malloc(sizeof(int) + sizeof(operation) + (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int));
	int flag = 1;
	memcpy(transaction,&flag, sizeof(int));
	memcpy(transaction + sizeof(int), op, sizeof(operation));
	memcpy(transaction + sizeof(int) + sizeof(operation), vectors, (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int));
	fwrite(transaction, 1, sizeof(int) + sizeof(operation) + (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int), fd);
	fclose(fd);
	free(transaction);
}

void end_transaction(){
	log_counters();

	//open the recovery file for reading and writing without truncating it
	//we want to keep at least the vector in it,
	//even if the transaction is successful in the even that we crash afterwards
	FILE *fd = fopen(recovery_file, "r+");
	int flag = 0;
	fwrite(&flag, 1, sizeof(int), fd);
	fclose(fd);
}

void log_counters(){
	FILE *fd = fopen(counters_file,"w");
	//write the LTS counter, object counter and operation index counter to the meta data file
	int counters[3];
	counters[0] = counter;
	counters[1] = object_counter;
	counters[2] = op_index_counter;
	fwrite((char *) counters, 3, sizeof(int), fd);
	fclose(fd);
}

void handle_print(char *sender)
{
	char *response = (char *)malloc((NUM_SERVERS + 1)*sizeof(int) + sizeof(action));
	int i, servers = 0;
	action act = PRINT;
	memcpy(response, &act, sizeof(action));

	//at least set myself to 1. not sure if we should be maintaining this elsewhere..
	current_partition_members[my_id] = 1;
	for (i = 1; i <= NUM_SERVERS; i++){
		if (current_partition_members[i] == 1){
			memcpy(response + sizeof(action) + (servers + 1)*sizeof(int), &i, sizeof(int));
			servers++;
		}
	}
	memcpy(response + sizeof(action), &servers, sizeof(int));
	
	send_to(sender, sizeof(action) + (servers + 1)*sizeof(int), response, 0);
	free(response);
}

int update_partition_logs(operation *op, int server_index, int offset){
	char filename[MAX_NAME + 1];
	//If the offset is not set the operation goes at the end of the file
	//otherwise we need to mark the now old operation as deleted,
	//and then write the new one at the end
	debug(1, 1, 1, "update_part_log: start\n");
	debug(1, 1, 1, "update_part_log: offset is %d and server_index is %d\n", offset, server_index);
	if (offset != VALUE_NOT_SET){
		assert(server_index != VALUE_NOT_SET);
		sprintf(filename, "%i_%i.log", my_id, server_index);
		FILE *fd = fopen(filename, "r+");
		fseek(fd, offset, SEEK_SET);
		operation single_op;
		memset((char*)&single_op, '\0', sizeof(operation));
		ret = fread(&single_op, sizeof(operation),1, fd);
		if(ret != 1)
			debug(1,1,1, "Didnt read the op here\n");
		else if(single_op.obj_id == op->obj_id)
		{
			fseek(fd, -sizeof(operation), SEEK_CUR);
			//mark operation as purged
			action act = PURGED;
			fwrite(&act, 1, sizeof(action), fd);
		}		
		fclose(fd);
	}
	sprintf(filename, "%i_%i.log", my_id, op->timestamp.id);
	FILE *fd = fopen(filename, "r+");
	
	if(!fd)
	{
		fd = fopen(filename, "w");
	}

	//write operation at the end
	fseek(fd, 0, SEEK_END);
	int new_offset = ftell(fd);
	debug(1, 1, 1, "update_part_log: new_offset is %d\n", new_offset);
	fwrite(op, 1, sizeof(struct operation), fd);
	fclose(fd);
	ops_logged++;
	return new_offset;
}

void handle_partition_change(int group_size){
	debug(0, 0, 1, "There was a partition change!\n");

	int i;
	for (i = 1; i <= NUM_SERVERS; i++){
		//initialize the current partition members to 0
		//these values will be updated as we begin to receive the other servers' vectors
		current_partition_members[i] = 0;
		reconciliation[i] = 1;
		max_values[i] = VALUE_NOT_SET;
	}
	//set my values appropriately
	current_partition_members[my_id] = 1;
	reconciliation[my_id] = 0;

	if (group_size == 1){
		debug(0, 0, 1, "I am alone in the partition!\n");
	}
	else{
		vectors_left =  group_size - 1;
		send_vector();
	}
}

void send_vector(){
	send_to("Control", (NUM_SERVERS + 1)*sizeof(int), (char *)vectors[my_id], VECTOR);
}

void recv_vector(char *sender, int vector[NUM_SERVERS + 1]){
	debug(1,1,1, "recv_vector: \n");
	//The first field in the vector contains the id of the server sending the vector
	int server_index = vector[0];
	//debug(0, 0, 1, "The received vector is from server %i and is [%i,%i,%i,%i,%i]\n", server_index,
	//		vector[1], vector[2], vector[3], vector[4], vector[5]);
	//We should always update the vectors matrix as the received vector can only have newer values
	memcpy(vectors[server_index], vector, (NUM_SERVERS + 1)*sizeof(int));
	write_matrix();

	current_partition_members[server_index] = 1;
	//We can create the server mapping as we know the server id and its private group name
	strcpy(mappings[server_index], sender);

	vectors_left--;

	if(vectors_left == 0){
		//start reconciliation

		int provider[NUM_SERVERS]; 		//an array that stores the index of the provider for each server's updates
		int min_values[NUM_SERVERS];	//an array that stores each server's smallest update value known to everyone
		int i, j;
		for (i = 1; i <= NUM_SERVERS; i++){
			min_values[i] = MAX_VALUE;
			provider[i] = VALUE_NOT_SET;
		}

		for (i = 1; i <= NUM_SERVERS; i++){
			//determine who has MAX and what is the MIN for server i
			for (j = 1; j <= NUM_SERVERS; j++){
				//we only want to compute these values for current partition members
				if (current_partition_members[j]){
					if (max_values[i] == VALUE_NOT_SET || max_values[i] < vectors[j][i]){
						max_values[i] = vectors[j][i];
						provider[i] = j;
						debug(0, 0, 1, "New max_val for %d is %d and provider is %d\n", i, max_values[i],j);
						//NOTE: ties are broken naturally as we iterate from lowest id to highest,
						//		and there can be only one provider that will be determined to be the same by each server
					}
					if (min_values[i] == MAX_VALUE || min_values[i] > vectors[j][i]){
						min_values[i] = vectors[j][i];
						debug(0, 0, 1, "New min_val for %d is %d and provider is %d\n", i, min_values[i],provider[i]);
					}
				}
			}
		}
		for (i = 1; i <= NUM_SERVERS; i++){
			if (provider[i] == my_id && min_values[i] < max_values[i]){
				//I am responsible for sending all updates for i between min_values[i] and max_values[i]
				//since I am the provider I do not need to be reconciled
				debug(0, 0, 1, "I am the provider for updates from server %i between %i and %i!\n", i, min_values[i], max_values[i]);
				reconciliation[i] = 0;

				//read from partition log for server i
				FILE *fd;
				char filename[MAX_NAME + 1];
				sprintf(filename, "%i_%i.log", my_id, i);
				fd = fopen(filename, "r");
				//we can start reading from the minimum value forward
				operation * single_op = (operation *)malloc(sizeof(operation));
				fread(single_op, 1, sizeof(operation), fd);
				while(single_op->op_index <= min_values[i])
				{
					fread(single_op, 1, sizeof(operation), fd);
				}			
				
				if(single_op->act != PURGED || (single_op->act == PURGED && single_op->op_index == max_values[i]))
				{
					if(single_op->act == PURGED && !feof(fd))
						debug(0, 0, 1, "Interestting.........p\n");
					
					debug(0, 0, 1, "Sending the first recon op\n");
					dump_op(single_op);
					send_to("Control", sizeof(operation), (char*)single_op, RECONCILIATION_OP);
				}
				free(single_op);

				operation *op = (operation *)malloc(MAX_READ*sizeof(operation));
				//NOTE: we read to the end of the file since we would need to send everything
				//		and there should not be any new updates stored there
				//		if this was not the case we could read up to max_values[i]
				while(!feof(fd)){
					int nread = fread(op, 1, MAX_READ*sizeof(operation), fd);
					assert (nread % sizeof(operation) == 0);
					/* fread returns a short count either at EOF or when an error occurred */
					if(nread < MAX_READ*sizeof(operation)) {
						if (!feof(fd)){
							printf("server: An error occurred during file reading...\n");
						}
					}
					int j;
					for (j = 0; j < nread / sizeof(operation); j++){
						if (op[j].act == PURGED){
							//do not send PURGED operations unless it is the last one
							if (feof(fd) && j == nread / sizeof(operation) - 1){
								send_to("Control", sizeof(operation), (char *)&op[j], RECONCILIATION_OP);
								debug(1,1,1,"sending reconciliation operation:\n");
								
								dump_op(&op[j]);
								assert(op[j].op_index == max_values[i]);
							}
						}
						else{
							debug(1,1,1,"sending reconciliation operation:\n");
							dump_op(&op[j]);
							send_to("Control", sizeof(operation), (char *)&op[j], RECONCILIATION_OP);
						}
					}
				}
				free(op);
			}
			else{
				//if I have less than the max value for server i, I need to be reconciled
				if (vectors[my_id][i] < max_values[i])
				{
					debug(0, 0, 1, "I need to be reconciled for server %i because i have %d and max is %d!\n", i, vectors[my_id][i], max_values[i]);
					reconciliation[i] = 1;
				}
				else
				{
					//otherwise I am up-to-date
					debug(0, 0, 1, "I am upto date for server %i!\n", i);
					reconciliation[i] = 0;
				}
			}
		}
	}
}

void send_matrix(short init_gc){
	if (init_gc)
		send_to("Control", (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int), (char *)vectors, PROPOSE_GC);
	else
		send_to("Control", (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int), (char *)vectors, MATRIX);
}

void recv_matrix(int matrix[NUM_SERVERS + 1][NUM_SERVERS + 1]){
	//NOTE: we should probably update our matrix if somebody knows more about somebody else than we do
	debug(1,1,1, "recv_matrix: \n");
	int i,j;
	debug(1,1,1, "\treceived matrix is: \n");
	//for (i = 1; i <= NUM_SERVERS; i++){
	//	debug(0, 0, 1, "Vector for %i is [%i,%i,%i,%i,%i]\n", i,
	//			matrix[i][1], matrix[i][2], matrix[i][3], matrix[i][4], matrix[i][5]);
	//}

	//update our matrix if we can
	for (i = 1; i <= NUM_SERVERS; i++){
		if (i == my_id)
			continue;
		for (j = 1; j <= NUM_SERVERS; j++){
			if (matrix[i][j] > vectors[i][j])
				vectors[i][j] = matrix[i][j];
		}
	}
	write_matrix();
}

void perform_gc(){
	//Our matrix should contain all the latest vectors that we know of
	int i,j, min_val, value_updated[NUM_SERVERS + 1];
	for(i = 0; i <= NUM_SERVERS; i++)
		value_updated[i] = 0;

	//Determine the minimum for each server and see if the gc_vector can be updated
	for (i = 1; i <= NUM_SERVERS; i++){
		min_val = MAX_VALUE;
		for (j = 1; j <= NUM_SERVERS; j++){
			if (min_val == MAX_VALUE || min_val > vectors[j][i])
				min_val = vectors[j][i];
		}
		if (gc_vector[i] == VALUE_NOT_SET || gc_vector[i] < min_val){
			gc_vector[i] = min_val;
			value_updated[i] = 1;
		}
	}

	for (i = 1; i <= NUM_SERVERS; i++){
		if (value_updated[i]){
			//At this point the gc_vector should contain all the op_indexes of the operations
			//up to and including which can be garbage collected from each of the logs
			//Also if delete always wins, deleted objects with last op with op_index lower than this can be garbage collected
			//NOTE: that the value of server i in gc_vector should be compared with the LTS id of the last op

			char filename[MAX_NAME + 1];
			debug(1, 1, 1, "*** real garbage collection beginning ***\n");
			debug(1, 1, 1, "removing server %d operations up-to %d\n", i, gc_vector[i]);

			sprintf(filename, "%i_%i.log", my_id, i);
			FILE *fd = fopen(filename, "r+");

			if(!fd)
			{
				debug(1, 1, 1, "log file %s does not exists\n", filename);
				continue;
			}
			else{
				fseek(fd, 0, SEEK_END);
				debug(1, 1, 1, "\t Opening discussion file %s of size %d.\n", filename, ftell(fd));
				fseek(fd, 0, SEEK_SET);
				//find the offset of the first operation to be kept
				operation *op = (operation *)malloc(sizeof(operation));
				memset((char*)op, '\0', sizeof(operation));
				fread(op, 1, sizeof(operation), fd);
				if (op->act == DELETE && op->op_index <= gc_vector[i]){
					debug(1, 1, 1, "\t Physically removing object %d, because 1st op in the file is DELETE.\n", op->obj_id);
					dump_op(op);
					gc_object(op);
				}

				while(!feof(fd) && op->op_index <= gc_vector[i]){
					fread(op, 1, sizeof(operation), fd);
					if (op->act == DELETE){
						debug(1, 1, 1, "\t Physically removing object %d, because some op in the file is DELETE.\n", op->obj_id);
						dump_op(op);
						gc_object(op);
					}
				}
				free(op);

				if (ftell(fd) == 0){
					fclose(fd);
					return;
				}
				else if(!feof(fd))
					fseek(fd, -sizeof(operation), SEEK_CUR);

				debug(1, 1, 1, "\t File pointer at first operation to be kept at position %d.\n", ftell(fd));
				operation *op_block = (operation *)malloc(MAX_READ*sizeof(operation));
				memset((char*)op_block, '\0', MAX_READ * sizeof(operation));

				//the file pointer should be at the first operation that needs to be kept
				char temp_filename[MAX_NAME + 1];
				sprintf(temp_filename, "%i_tempfile", my_id);
				FILE *temp_file = fopen(temp_filename,"w");
				//read from the log and write to the temp file
				//once we are done reading we can rename the temp file to our log file
				while(!feof(fd)){
					debug(1, 1, 1, "\t BEfore read : File pointer at position %d.\n", ftell(fd));
					int nread = fread(op_block, 1, MAX_READ * sizeof(operation), fd);
					debug(1, 1, 1, "\t After read : File pointer at position %d.\n", ftell(fd));
					debug(1, 1, 1, "\t Read-in %d bytes.\n", nread);
					//dump_op(op);
					if (nread < MAX_READ*sizeof(operation)){
						assert(feof(fd));
						if(ferror(fd))
							perror("Our I/O had an error\n");
						fwrite(op_block, nread/sizeof(operation),sizeof(operation), temp_file);
					}
					else
						fwrite(op_block, 1, MAX_READ * sizeof(operation), temp_file);
				}
				fclose(fd);
				rename(temp_filename,filename);
				fclose(temp_file);
				free(op_block);
			}
		}
	}
}

void gc_object(operation *op){
	debug(1, 1, 1, "gc_object: Starting...Operation under review:\n");
	dump_op(op);

	char disc_filename[MAX_NAME + 1];
	sprintf(disc_filename, "%i_", my_id);
	strcat(disc_filename,op->disc_name);
	FILE *fd = fopen(disc_filename,"r");

	assert(fd);

	fseek(fd, 0, SEEK_END);
	int file_size = ftell(fd);
	debug(1, 1, 1, "\t Opened discussion file %s.\n", disc_filename);
	assert(file_size % sizeof(object) == 0);
	int num_objects = file_size/sizeof(object);
	object *disc = (object*)malloc(file_size);
	fseek(fd, 0, SEEK_SET);
	//read-in the entire discussion
	fread(disc, 1, file_size, fd);
	fclose(fd);
	debug(1, 1, 1, "\t This is the disc we have on file with %d objects.\n", num_objects);
	dump_disc(num_objects, disc);

	int i;
	for (i = 0; i < num_objects; i++){
		if (disc[i].id == op->obj_id){
			//remove the object from the discussion

			object *new_disc = (object *)malloc(file_size - sizeof(object));
			memcpy(new_disc, disc, i*sizeof(object));
			memcpy(new_disc + i, disc + i + 1, (num_objects - i - 1)*sizeof(object));

			fd = fopen(disc_filename, "w");
			fwrite(new_disc,  (num_objects - 1), sizeof(object), fd);
			fclose(fd);

			free(disc);
			free(new_disc);
			return;
		}
	}
}

void recover(){
	debug(1,1,1, "recover: starting recovery...\n");
	FILE *fd = fopen(recovery_file, "r");
	if (fd){
		debug(1,1,1, "\t recovery file exists...\n");
		char * transaction = (char *)malloc(sizeof(int) + sizeof(operation) + (NUM_SERVERS + 1) * (NUM_SERVERS + 1) * sizeof(int));
		int flag;
		fread(transaction, 1, sizeof(int) + sizeof(operation) + (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int), fd);

		FILE *fd_counters;
		fd_counters = fopen(counters_file, "r");
		int counters[3];
		counters[0] = counters[1] = counters[2] = 0;
		if (fd_counters){
			debug(1,1,1, "\t counters file exists, setting the counters here...\n");
			fread(counters, sizeof(int), 3, fd_counters);
			fclose(fd_counters);

			counter = counters[0];
			object_counter = counters[1];
			op_index_counter = counters[2];
		}

		memcpy(&flag, transaction, sizeof(int));
		debug(1,1,1, "\t the flag in the transaction file is %i...\n", flag);
		memcpy(vectors, transaction + sizeof(int) + sizeof(operation), (NUM_SERVERS + 1)*(NUM_SERVERS + 1)*sizeof(int));
		//debug(1,1,1, "\tmy vector is [%i,%i,%i,%i,%i]...\n", vectors[my_id][1], vectors[my_id][2], vectors[my_id][3], vectors[my_id][4], vectors[my_id][5]);

		if (flag){
			debug(1,1,1, "\t there was an unfinished transaction...\n");
			operation op;
			memcpy(&op, transaction + sizeof(int), sizeof(operation));
			if (counter < op.timestamp.counter){
				debug(1,1,1, "\t the LTS counter of the operation is higher than the counter that was stored...\n");
				counter = op.timestamp.counter;
			}

			if (op.act == APPEND && op.append_lts.id == my_id){
				int obj_id_counter = op.obj_id >> 3;
				if (counters[1] < obj_id_counter){
					debug(1,1,1, "\t the object_id of the operation is higher than the object_counter that was stored...\n");
					object_counter = obj_id_counter;
				}
			}

			if (counters[2] < op.op_index){
				debug(1,1,1, "\t the op_index of the operation is higher than the op_index_counter that was stored...\n");
				op_index_counter = op.op_index;
			}

			apply_recover_op(&op);
			//We do not need to send this operation to the control group as there will be reconciliation once we reappear
			//We also do not need to send anything to the clients as we probably do not have any clients
			//connected to us right after recovering
			if (vectors[my_id][op.timestamp.id] < op.op_index){
				vectors[my_id][op.timestamp.id] = op.op_index;
				write_matrix();
			}
			//We need to mark the transaction as ended
			end_transaction();
		}
		free(transaction);
		fclose(fd);
	}
	else{
		debug(1,1,1, "The recovery file %s does not exist, we must be starting for the first time\n", recovery_file);
		//TODO: We may also want to initialize some things here, rather than in main
		int i, j;
		for (i = 1; i <= NUM_SERVERS; i++){
			for(j = 1; j <= NUM_SERVERS; j++)
				vectors[i][j] = VALUE_NOT_SET;
			gc_vector[i] = VALUE_NOT_SET;
		}
		//When the vector is received the recipient will know the private group of the sender
		//since we have one extra space in the vector at position 0, we can communicate our id there
		vectors[my_id][0] =  my_id;
	}
}

void handle_sync(char * sender)
{
	debug(1,1,1, "Handling SYNC sent by %s\n", sender);
	
	ret = SP_multicast( Mbox, AGREED_MESS, sender, SYNC_ACK, 0, NULL);
	if(!check_ret(ret))
	{
		printf("Connection to group lost in handle_sync.\n");
		return;
	}
}
void handle_sync_ack()
{
	syncs_left--;
	debug(1,1,1, "SYNC ACK recvd. %d syncs remaining\n", syncs_left);
	if(syncs_left == 0)
	{
		msgs_sent = 0;
		sync_sent = 0;
	}
}
