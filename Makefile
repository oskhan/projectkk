CC=gcc
LD=gcc
CFLAGS=-g -O0 -Wall 
CPPFLAGS=-I. -Iinclude
SP_LIBRARY_DIR=/home/cs437/exercises/ex3

all: client server  

client.o: struct.h client.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c client.c

server.o: struct.h server.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c server.c

misc.o: struct.h Makefile misc.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c misc.c

client:  $(SP_LIBRARY_DIR)/libspread-core.a client.o misc.o
	$(LD) misc.o client.o -o $@ $(SP_LIBRARY_DIR)/libspread-core.a 

server:  $(SP_LIBRARY_DIR)/libspread-core.a server.o misc.o
	$(LD) misc.o server.o -o $@ $(SP_LIBRARY_DIR)/libspread-core.a 

clean:
	rm -f *.o client server

