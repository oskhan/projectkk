#include "struct.h"

// the vars we need to multicast send/rcv 

char    User[MAX_LINE];
char    Spread_name[MAX_LINE];
char    Private_group[MAX_GROUP_NAME];
char    Server_Private_group[MAX_GROUP_NAME] = "";
char    group[MAX_GROUP_NAME];  //our group name
mailbox Mbox;

int		ret;
int		service_type = 0, num_groups;
char	ret_groups[MAX_CLIENTS][MAX_GROUP_NAME];
char	sender[MAX_GROUP_NAME];
char	recv_mess[MAX_BYTES] ;
int16	dummy_mess_type;
int		dummy_endian_mismatch;

discussion 	disc;	//the discussion this client is currently participating in
int 		connected = 0;
int			server_index = VALUE_NOT_SET;
int 		joined = 0;

int check_ret(int );
void generate_top_menu();
void recv_server_resp();
void print_discussion();
void User_command();

void update_line_nums(obj_node * node, int);
int validate_input(char*);

void init_op(operation *);
void dealloc_disc();

void handle_connect(int);
void apply_connect(char *);

void handle_join(char *);
void apply_join(char *);

void handle_append(char *);
void apply_append(char *);

void handle_edit(char * line, int index);
void apply_edit(char*);

void handle_delete(int index);
void apply_delete(char *);

void handle_print();
void apply_print(char *);

int main()
{

	sprintf( User, "projectkkClient" );
    sprintf( Private_group, "projectkk" );
    sprintf( group, "kanovkhan" );
    sprintf( Spread_name, "%i@localhost", PORT);

	printf("client: connecting to %s\n\n", Spread_name );
	//the clients should probably not get membership messages
	//although this would have been a convinient way to check if the server has left\partitioned\crashed\disconnected
	ret = SP_connect( Spread_name, User, 0, 1, &Mbox, Private_group ) ;
	check_ret(ret);

	E_init();
	E_attach_fd( 0, READ_FD, User_command, 0, NULL, LOW_PRIORITY );

	E_attach_fd( Mbox, READ_FD, recv_server_resp, 0, NULL, HIGH_PRIORITY );
	printf("*********Main Menu*********\n\n");
	generate_top_menu();

	printf("Command: ");
	fflush(stdout);
	E_handle_events();

	return 0;
}

void User_command(){
	char command, *line = (char*)malloc(MAX_LINE + 1), *input = (char*)malloc(MAX_LINE + 4);
	unsigned int index = 0, size = MAX_LINE;

	getline(&input, &size, stdin);
	input[strlen(input) - 1] = '\0';
	debug(1, 1, 1, "The input was %s and had %d chars and size is %d\n", input, strlen(input), size);

	command = input[0];

	if(command == 'q')
	{
		printf("Sorry to see you go...\n");
		exit(0);
	}
	switch(command)
	{
		case 'c':
		{
			if(validate_input(input))
			{
				index = atoi(input + 2);
				debug(0, 0, 1, "You are trying to connect to %d\n", index);
				handle_connect(index);
			}
			break;
		}
		case 'j':
		{
			if(!connected)
			{
				printf("You must be connected to a server to access this functionality.\n");
				break;
			}
			strcpy(line, input + 2);
			if( strlen(line) < 1 )
			{
				printf("Invalid discussion \n");
				break;
			}
			handle_join(line);
			break;
		}		
		case 'p':
		{
			if(!connected)
			{
				printf("You must be connected to a server to access this functionality.\n");
				break;
			}
			handle_print();
			break;
		}
		case 'a':
		{
			if(!connected)
			{
				printf("You must be connected to a server to access this functionality.\n");
				break;
			}
			if(!joined)
			{
				printf("You must be joined to a discussion to access this functionality.\n");
				break;
			}
			//getline(&line, &size, stdin);
			if (strlen(input) < 3){
				printf("You must provide the line contents\n");
				generate_top_menu();
				break;
			}
			//input[strlen(input) - 1] = '\0';
			strcpy(line, input + 2);
			debug(1, 1, 1, "The input was %s and had %d chars and size is %d\n", line, strlen(line), size);
			debug(0, 0, 1, "You are trying to append %s\n", line);
			handle_append(line);
			break;
		}
		case 'e':
		{
			if(!connected)
			{
				printf("You must be connected to a server to access this functionality.\n");
				break;
			}
			if(!joined)
			{
				printf("You must be joined to a discussion to access this functionality.\n");
				break;
			}
			if(validate_input(input))
			{
				index = atoi(input + 2);
				printf("Enter edited version of line: ");
				getline(&line, &size, stdin);
				line[strlen(line) - 1] = '\0';
				handle_edit(line, index);
			}
			break;
		}
		case 'd':
		{
			if(!connected)
			{
				printf("You must be connected to a server to access this functionality.\n");
				break;
			}
			if(!joined)
			{
				printf("You must be joined to a discussion to access this functionality.\n");
				break;
			}
			if(validate_input(input))
			{
				index = atoi(input + 2);
				debug(0, 0, 1, "You are trying to delete line %d\n", index);
				handle_delete(index);
			}
			break;
		}
		default:
		{
			printf("unrecognized command: %c\n", command);
			print_discussion();
			generate_top_menu();
			printf("Command: ");
			fflush(stdout);
		}

	}
}

int check_ret(int ret)
{
	if(ret == GROUPS_TOO_SHORT || ret == BUFFER_TOO_SHORT)
	{
		debug(0, 0, 1, "group/buffer too short error!!\n");
		exit(1);
	}
	else if(ret == CONNECTION_CLOSED)
	{
		debug(0, 0, 1, "Connection closed!!\n");
		printf("Connection closed!!\n");
		exit(0);
	}
	else if( ret < 0 )
	{
		SP_error( ret );
		exit(1);
	}

	return 1;
}

void generate_top_menu()
{
	printf("- [c]onnect to server (enter server index along with command)\n");
	printf("- [j]oin discussion board (enter discussion board name along with command)\n");
	printf("- [a]ppend a line (enter line along with command)\n");
	printf("- [e]dit a line (enter line number along with command)\n");
	printf("- [d]elete a line (enter line number along with command)\n");
	printf("- [p]rint the current view of servers\n");
	printf("- [q]uit\n\n");

}

void print_discussion()
{
	obj_node * node = disc.head;
	debug(1, 1, 1, "print_discussion: start\n\n");

	if (strlen(disc.name) > 0)
		printf("Discussion \"%s\"\n", disc.name);

	while(node)
	{
		if(!node->obj.visible)
		{
			node = node->next;
			continue;
		}
		printf("%d. %s\n", node->line_num, node->obj.line);
		node = node->next;
	}

	printf("\n");
}

void update_line_nums(obj_node * node, int append)
{
	//TODO: Does not seem to work if the node deleted is the last one and there is an append afterwards
	obj_node *iter = NULL, * copy_node = node;
	int seq = 0;

	if(append)
		node = node->prev;

	//lets look back and find the parent
	while(node != NULL && node->obj.visible == 0)
	{
		node = node->prev;
	}

	//should the new node be the head of the visible list?
	if(node == NULL)
	{
	
		if(append)
		{
			debug(1, 1, 1, "update_line: New node is the new head with line num 1\n");
			copy_node->line_num = 1;
		}
		else
		{
			debug(1, 1, 1, "update_line: New node is a delete node and has no parent. Lets see if we have other visible child nodes...\n");
			copy_node->line_num = 0;
		}
		seq = copy_node->line_num;
		
		//the head has been stamped with correct line num, so move on to next guy and start stamping...
		copy_node = copy_node->next;

		while(copy_node != NULL)
		{
			if(copy_node->obj.visible == 0)
			{
				copy_node = copy_node->next;
				continue;	
			}
			copy_node->line_num = ++seq;
			copy_node = copy_node->next;
		}
	}
	else // the new node should be somewhere after the head of hte visible list
	{
		//node is the parent of new node

		seq = node->line_num;
		iter = node;
		debug(1, 1, 1, "update_line: Found parent with line num %d and line %s\n", seq, node->obj.line);
		while(iter)
		{
			iter = iter->next;
			if(!iter)
				return;
			if(iter->obj.visible == 0)
				continue;
			iter->line_num = ++seq;
		}
	}
	
}

void init_op(operation * op)
{
	memset(op, '\0', sizeof(operation));
	int i;
	op->act = PURGED;
	op->timestamp.id = VALUE_NOT_SET;
	op->timestamp.counter = VALUE_NOT_SET;
	op->op_index = VALUE_NOT_SET;
	op->append_lts.id = VALUE_NOT_SET;
	op->append_lts.counter = VALUE_NOT_SET;
	strcpy(op->new_val, "");
	for(i = 0; i < MAX_LINE + 1; i++)
		op->new_val[i] = '\0';
	op->obj_id = VALUE_NOT_SET;
	strcpy(op->disc_name, "");
	for(i = 0; i < MAX_NAME + 1; i++)
		op->disc_name[i] = '\0';
}

int validate_input(char * input)
{
	int max_val = 0;
	if(input[0] == 'c')
	{
		if (strlen(input) < 3){
			printf("You must provide the server index to connect to.\n");
			generate_top_menu();
			printf("Command: ");
			fflush(stdout);
			return 0;
		}
	}
	else if(input[0] == 'e' || input[0] == 'd')
	{
		obj_node * temp = disc.tail;
		while(temp != NULL && temp->obj.visible == 0)
			temp = temp->prev;
		
		if(temp == NULL)
		{
			max_val = 0;
			printf("Nothing to edit/delete. Please enter correct input.\n");
			generate_top_menu();
			printf("Command: ");
			fflush(stdout);
			return 0;
		}
		else
			max_val = temp->line_num;

		int index;
		index = atoi(input + 2);

		if(index > max_val)
		{
			printf("Invalid line number. Please enter correct input.\n");
			generate_top_menu();
			printf("Command: ");
			fflush(stdout);
			return 0;
		}
	}
	return 1;
}
int send_to_server(char * to, int len, char * msg)
{
	ret = SP_multicast( Mbox, AGREED_MESS, to, 0, len, msg );
	if(!check_ret(ret))
	{
		//TODO: This probably still needs to happen!!!
		//		We should also remove the server info, i.e. server_index, Server_Private_Group, and set connected and joined to 0
		printf("Connection to server lost, dropping discussion and returning to main menu.\n");
		return 0 ;
	}

	return 1;
}

void recv_server_resp()
{
	action act;
	char * response;
	int i;
	service_type = 0;
    ret = SP_receive( Mbox, &service_type, sender, MAX_CLIENTS, &num_groups, ret_groups,
                                  &dummy_mess_type, &dummy_endian_mismatch, sizeof(recv_mess), recv_mess );
	check_ret(ret);

	if(Is_membership_mess(service_type))
	{
		debug(1, 1, 1, "Recvd a memb msg\n");
		if(Is_caused_disconnect_mess(service_type) || Is_caused_network_mess(service_type))
		{
			debug(1, 1, 1, "Recvd a memb msg due to network or disconnect\n");
			membership_info memb;
			SP_get_memb_info(recv_mess, service_type, &memb);

			int condition = 0;

			if(Is_caused_disconnect_mess(service_type))
			{
				if(strcmp(memb.changed_member, Server_Private_group) == 0)
					condition = 1;
			}
			else
			{
				for(i = 0; i < num_groups; i++)
				{	
					if(strcmp(ret_groups[i], Server_Private_group) == 0)
					{
						break;
					}
				}
				if(i == num_groups)
					condition = 1;
			}

			if(condition)
			{
				printf("You were disconnected from the server. If you want to continue using our excellent services, please connect to another server.\n");
				
				if(connected)
				{
					connected = 0;
					char temp[MAX_NAME] = "";
					sprintf(temp, "ServerMemb%d", server_index);
					SP_leave(Mbox, temp);
				}
				if(joined)
				{
					//deallocate the current discussion
					dealloc_disc();
				}
				joined = 0;
			}
		}
		return;
	}

	memcpy(&act, recv_mess, sizeof(action));
	
	debug(1, 1, 1, "Recvd a server response with action %d\n", act);
	response = recv_mess + sizeof(action);

	if(act == CONNECT)
	{
		apply_connect(response);
	}
	else if(act == JOIN)
	{
		apply_join(response);
		print_discussion();
	}
	else if(act == APPEND)
	{
		apply_append(response);
		print_discussion();
	}
	else if(act == EDIT)
	{
		apply_edit(response);
		print_discussion();
	}
	else if(act == DELETE)
	{
		apply_delete(response);
		print_discussion();
	}
	else if(act == PRINT)
	{
		apply_print(response);
	}
	else
		printf("Unrecognized response from server!\n");
	printf("Command: ");
	fflush(stdout);

}
void handle_connect(int index)
{
	if (connected){
		debug(1,1,1, "handle_connect: Was already connected to another server! Should probably leave his group.\n");
		char server_group[MAX_GROUP_NAME];
		memset(server_group, '\0', MAX_GROUP_NAME);
		sprintf(server_group, "ServerMemb%d", server_index);
		SP_leave(Mbox, server_group);
		connected = 0;
		if(joined)
		{
			//deallocate the current discussion
			dealloc_disc();
		}
		joined = 0;
	}

	operation conn_op;
	init_op(&conn_op);
	conn_op.act = CONNECT;
	strcpy(conn_op.new_val, Private_group);

	debug(1, 1, 1, "handle_connect: The private groups is %s and len is %d\n", Private_group, strlen(Private_group));

	memset(group, '\0', MAX_GROUP_NAME);

	//create the group name on which the server will be listening for new clients
	sprintf(group, "Server%d", index);

	//we need to preserve the index of the server which we are connecting to
	server_index = index;

	dump_op(&conn_op);

	send_to_server(group, sizeof(operation), (char*)&conn_op);
}

void apply_connect(char * response)
{
	strcpy(Server_Private_group, response);
	connected = 1;
	debug(0, 0, 1, "Connected to server with private group \"%s\"\n", Server_Private_group);
	printf("Connected to server %d\n", server_index);
	char server_group[MAX_GROUP_NAME];
	memset(server_group, '\0', MAX_GROUP_NAME);
	sprintf(server_group, "ServerMemb%d", server_index);
	SP_join(Mbox, server_group);
	debug(0, 0, 1, "Joined group %s!!\n", server_group);
}

void handle_join(char * line)
{
	operation join_op; 

	init_op(&join_op);
	
	join_op.act = JOIN;
	strcpy(join_op.disc_name, line);

	send_to_server(Server_Private_group, sizeof(operation), (char*)&join_op);	
}

void dealloc_disc()
{
	obj_node * temp = disc.head, *next;
	while(temp)
	{
		next = temp->next;
		free(temp);
		temp = next;
	}
}

//response contains a discussion followed by a list of objects
void apply_join(char * response)
{
	int i, seq = 0;
	obj_node * temp;
	char * start = response;
	
	if(joined)
	{
		//deallocate the current discussion
		dealloc_disc();
	}
	
	memcpy(&disc, response, sizeof(discussion));	
	
	start += sizeof(discussion);

	debug(0, 1, 1, "Joining discussion name %s with %d objects\n", disc.name, disc.num_objs);
	
	disc.head = disc.tail = NULL;

	for(i = 0; i < disc.num_objs; i++)
	{

		temp = (obj_node*)malloc(sizeof(obj_node));
		memset((char*)temp, '\0', sizeof(obj_node));
		temp->next = temp->prev = NULL;
		memcpy(&temp->obj, start, sizeof(object));

		start += sizeof(object);

		//apply line num if visible
		if(temp->obj.visible)
			temp->line_num = ++seq;

		debug(0, 1, 0, "Recvd object %s\n", temp->obj.line);	
		
		if(i == 0)
		{
			disc.head = temp;	
			disc.tail = disc.head;
		}
		else
		{

			//create the two links needed to attach a new node
			disc.tail->next = temp;
			temp->prev = disc.tail;
			disc.tail = temp;
		}
	}
	joined = 1;
}

void handle_append(char * line)
{
	operation append_op; 

	init_op(&append_op);
	
	append_op.act = APPEND;
	strcpy(append_op.new_val, line);
	strcpy(append_op.disc_name, disc.name);
	
	send_to_server(Server_Private_group, sizeof(operation), (char*)&append_op);	

}

void apply_append(char * response)
{
	obj_node * temp;
	obj_node * append_node = (obj_node*)malloc(sizeof(obj_node));	
	append_node->next = append_node->prev = NULL;

	memcpy(&append_node->obj, response, sizeof(object));	

	//see where this needs to go in the list (according to append_LTS)
	//starting from the tail backwards 
	temp = disc.tail;
	
	//is the discussion empty?
	if(!temp)
	{
		disc.tail = disc.head = append_node;
		append_node->next = NULL;
		append_node->line_num = 1;
		return;
	}

	//keep going back in the list till temp points to the would be parent of the appended node
	while(temp != NULL && compare_LTS(&temp->obj.last_op.append_lts, &append_node->obj.last_op.append_lts) != SECOND_GREATER)
		temp = temp->prev;

	//should the appended node be the new head?
	if(!temp)
	{
		append_node->next = disc.head;
		disc.head->prev = append_node;
		disc.head = append_node;
	}
	else
	{
		//only if there is another node below temp (which is the parent), could there be a duplicate
		if(temp->next)
		{
			//just making sure there is no duplicate there
			if(compare_LTS(&append_node->obj.last_op.append_lts, &temp->next->obj.last_op.append_lts) == EQUAL)
			{
				debug(1, 1, 1, "Detected duplicate append lts id %d counter %d!! Ignored...\n", temp->next->obj.last_op.append_lts.id, temp->next->obj.last_op.append_lts.counter);
				return;
			}
		}

		//lets make the connections to insert it into the list
		append_node->next = temp->next;
		append_node->prev = temp;
		temp->next = append_node;

		//if this is within the list somewhere
		if(append_node->next)
		{
			append_node->next->prev = append_node;
			
		}else
		{
			//the new node should be the tail
			disc.tail = append_node;
		}
	}
	update_line_nums(append_node, 1);
}

void handle_edit(char * line, int index)
{
	operation op;
	init_op(&op);
	obj_node * edit_node = disc.tail;
	
	assert(edit_node != NULL);

	//TODO: We should make sure that a valid index was provided
	while(edit_node->line_num != index || !edit_node->obj.visible)
	{
		edit_node = edit_node->prev;
	}

	op.act = EDIT;
	op.obj_id = edit_node->obj.id;
	strcpy(op.new_val, line);
	strcpy(op.disc_name, disc.name);

	send_to_server(Server_Private_group, sizeof(operation), (char*)&op);	
}

void apply_edit(char * response)
{
	obj_node *iter = NULL, * edit_node = disc.tail, *new_node;
	object * obj = (object*) response;
	int updated = 0;	

	assert(edit_node != NULL);

	while(edit_node != NULL && edit_node->obj.id != obj->id)
	{
		edit_node = edit_node->prev;
	}
	
	if(edit_node == NULL)
	{
		//wasnt found
		debug(1, 1, 1, "Did not find the obj to be edited. Creating one...\n");
		dump_obj(obj);	
	
		new_node = (obj_node*)malloc(sizeof(obj_node));
		iter = disc.tail;
		while(iter != NULL && compare_LTS(&iter->obj.last_op.append_lts, &obj->last_op.append_lts) != SECOND_GREATER)
		{
			iter = iter->prev;	
		}
		if(iter == NULL)
		{
			//new node should be head
			
			debug(1, 1, 1, "The new obj is the new head.\n");
			new_node->next = disc.head;
			new_node->prev = NULL;
			if(disc.head)
				disc.head->prev = new_node;
			else
				disc.tail = new_node;
			disc.head = new_node;
		
			//edit_node = new_node;
		}
		else if(iter->next) //has both parent and child
		{
			assert(compare_LTS(&iter->obj.last_op.append_lts, &obj->last_op.append_lts) != 0);
			debug(1, 1, 1, "The new obj has both parent and child.\n");
			new_node->next = iter->next;
			new_node->next->prev = new_node;
			new_node->prev = iter;
			iter->next = new_node;
					
		}
		else //has no chile = last in listt
		{
			debug(1, 1, 1, "The new obj has no child, so must be last in list.\n");
			new_node->next = iter->next;
			new_node->prev = iter;
			iter->next = new_node;
			disc.tail = new_node;
		}

		memcpy(&new_node->obj, response, sizeof(object));	
		update_line_nums(new_node, 1);
	}
	else
	{
		debug(1, 1, 1, "Found the obj to be edited.\n");
		if(edit_node->obj.visible == 0)
			updated = 1;
		memcpy(&edit_node->obj, response, sizeof(object));	
		if(updated)
			update_line_nums(edit_node, 1);
	}
}

void handle_delete(int index)
{
	operation op;
	init_op(&op);
	obj_node * del_node = disc.tail;

	debug(1, 1, 1, "handle_delete: Start looking backwards from the tail which has line num %d and line %s and visible %d\n", del_node->line_num, del_node->obj.line, del_node->obj.visible);
	
	assert(del_node != NULL);

	while(del_node->line_num != index || !del_node->obj.visible)
	{
		del_node = del_node->prev;
	}

	op.act = DELETE;
	op.obj_id = del_node->obj.id;
	strcpy(op.disc_name, disc.name);
	
	send_to_server(Server_Private_group, sizeof(operation), (char*)&op);	

}

void apply_delete(char * response)
{
	obj_node  *iter, * new_node;
	object * obj = (object*) response;

	obj_node * del_node = disc.tail;
	

	while(del_node != NULL && del_node->obj.id != obj->id)
	{
		del_node = del_node->prev;
	}

	if(del_node == NULL)
	{
		//wasnt found
		debug(1, 1, 1, "Did not find the obj to be deleted. Creating one...\n");
		dump_obj(obj);	
	
		new_node = (obj_node*)malloc(sizeof(obj_node));
		iter = disc.tail;
		while(iter != NULL && compare_LTS(&iter->obj.last_op.append_lts, &obj->last_op.append_lts) != SECOND_GREATER)
		{
			iter = iter->prev;	
		}
		if(iter == NULL)
		{
			//new node should be head
			
			debug(1, 1, 1, "The new obj is the new head.\n");
			new_node->next = disc.head;
			new_node->prev = NULL;
			if(disc.head)
				disc.head->prev = new_node;
			else
				disc.tail = new_node;
			disc.head = new_node;
		
			//edit_node = new_node;
		}
		else if(iter->next) //has both parent and child
		{
			assert(compare_LTS(&iter->obj.last_op.append_lts, &obj->last_op.append_lts) != 0);
			debug(1, 1, 1, "The new obj has both parent and child.\n");
			new_node->next = iter->next;
			new_node->next->prev = new_node;
			new_node->prev = iter;
			iter->next = new_node;
					
		}
		else //has no chile = last in listt
		{
			debug(1, 1, 1, "The new obj has no child, so must be last in list.\n");
			new_node->next = iter->next;
			new_node->prev = iter;
			iter->next = new_node;
			disc.tail = new_node;
		}

		memcpy(&new_node->obj, response, sizeof(object));	
		update_line_nums(new_node, 1);
	}
	else
	{
		debug(1, 1, 1, "Found the obj to be deleted.\n");
		assert(del_node->obj.visible);
		del_node->obj.visible = 0;

		update_line_nums(del_node, 0);
	}
	
}

void handle_print()
{
	operation op;
	init_op(&op);	
	op.act = PRINT;
	
	send_to_server(Server_Private_group, sizeof(operation), (char*)&op);	
}

void apply_print(char * response)
{
	int i, num_servers, id;
	char * start = response;

	memcpy(&num_servers, start, sizeof(int));
	debug(1,1,1,"Got print cmd with %d servers\n", num_servers);
	
	printf("\nCurrent Partition: \n");

	for(i = 0; i < num_servers; i++)
	{
			
		start += sizeof(int);
		memcpy(&id, start, sizeof(int));
		printf("Server %d\n", id);
	}	
}
