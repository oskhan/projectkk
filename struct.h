#ifndef OSAMA
#define OSAMA

#define _GNU_SOURCE

#include <sys/select.h>
#include <assert.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include "sp.h"

struct object;

#define MAX_BYTES 	sizeof(object)*1000 
#define MAX_LINE 	80
#define MAX_NAME 	100
#define NUM_SERVERS 5
#define MAX_READ 	1000 //Maximum number of operations that can be read-in
#define MAX_CLIENTS 	1000 //Maximum number of operations that can be read-in
#define GC_THRESHOLD	1000
#define SYNC_THRESHOLD	100 

#define PORT		10010

#define FIRST_GREATER 	-1
#define SECOND_GREATER 	 1
#define EQUAL 			 0
#define VALUE_NOT_SET 	-1
#define MAX_VALUE		-2 //This can be used as "infinity"

// the debug levels - we can programmatically change them at run time
// i guess the idea being that if during computation we detect that things
// might not be 100% correct, we could increase the logging level
extern int D_IN_OUT;
extern int D_INTER;
extern int D_PROGRESS;

typedef enum action {APPEND, EDIT, DELETE, CONNECT, JOIN, PRINT, PURGED} action;
typedef enum message_type {SYNC, SYNC_ACK, VECTOR, PROPOSE_GC, MATRIX, OPERATION, RECONCILIATION_OP} message_type;

typedef struct __attribute__ ((__packed__)) LTS
{
	int id;
	int counter;
}LTS;

typedef struct __attribute__ ((__packed__)) operation
{
	action act;
	LTS timestamp;
	int op_index;
	LTS append_lts;
	char new_val[MAX_LINE + 1];
	int obj_id;
	char disc_name[MAX_NAME + 1];
}operation;

typedef struct __attribute__ ((__packed__)) object
{
	int id;
	char line[MAX_LINE + 1];
	short visible;
	operation last_op; //only needed on clients
	int logfile_id;
	int offset; //could be set to VALUE_NOT_SET if not present in log
}object;

//only needed on clients
typedef struct __attribute__ ((__packed__)) obj_node
{
	object obj;
	int line_num;
	struct obj_node *next, *prev;	
}obj_node;

//only needed on clients?
typedef struct __attribute__ ((__packed__)) discussion
{
	int num_objs;
	char name[MAX_NAME + 1];
	obj_node *head, *tail;	
}discussion;

typedef struct __attribute__ ((__packed__)) client_node
{
	char client_private_group[MAX_NAME + 1];
	struct client_node *next;
}client_node;

//doesnt need to be on disk because after the server comes back up,
//clients will have to reconnect
typedef struct __attribute__ ((__packed__)) disc_info
{
	char name[MAX_NAME + 1];
	client_node *name_list;
}disc_info;

typedef struct __attribute__ ((__packed__)) disc_node
{
	disc_info disc;
	struct disc_node *next;
}disc_node;

typedef struct __attribute__ ((__packed__)) operation_node
{
	operation op;
	struct operation_node *next;
}operation_node;

typedef struct __attribute__ ((__packed__)) queue_node
{
	char recipient[MAX_NAME];
	char * msg;
	int len;
	int16 tag;
	struct queue_node *next;
}queue_node;

//this can store the meta-data i guess?
typedef struct __attribute__ ((__packed__)) state
{
	int lts_counter;
	int obj_counter;
	int num_disussions;
	char ** discussions;
	int offsets[NUM_SERVERS];
}state;

extern void debug(int l1, int l2, int l3, const char *format, ...);
extern int compare_LTS(LTS * first, LTS * second);
extern void copy_LTS(struct LTS * dest, struct LTS * src);
extern void dump_op(operation *);
extern void dump_obj(object *);
extern void dump_disc(int num_objs, object * objs);

#endif
