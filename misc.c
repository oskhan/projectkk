#include "struct.h"

int D_IN_OUT = 1;
int D_INTER = 1;
int D_PROGRESS = 1;

void debug(int l1, int l2, int l3, const char *format, ...)
{
#ifdef DEBUG
	if( (l1 && D_IN_OUT) || (l2 && D_INTER) || (l3 && D_PROGRESS))
	{
		va_list ap;
		va_start(ap, format);
		vprintf(format, ap);
		va_end(ap);
	}
#endif
}

// compares the two LTSs, and returns -1 if the first one is greater,
// returns 1 if the second one is greater and 0 if equal
// NOTE: IT DOES NOT CONSIDER WRAP AROUND!
int compare_LTS(LTS * first, LTS * second)
{
        if(first->counter > second->counter)
                return FIRST_GREATER;
        else if(first->counter < second->counter)
                return SECOND_GREATER;
        else if(first->id > second->id)
                return FIRST_GREATER;
        else if(first->id < second->id)
                return SECOND_GREATER;
        else
                return EQUAL;
}

void copy_LTS(struct LTS * dest, struct LTS * src)
{
        dest->id = src->id;
        dest->counter = src->counter;
}

void dump_op(operation * op)
{
        debug(1, 1, 1, "Operation: \n");
        debug(1, 1, 1, "\tact: %d\n", op->act);
        debug(1, 1, 1, "\tlts id %d count %d\n", op->timestamp.id, op->timestamp.counter);
        debug(1, 1, 1, "\top_indx %d\n", op->op_index);
        debug(1, 1, 1, "\tappend lts id %d count %d\n", op->append_lts.id, op->append_lts.counter);
        debug(1, 1, 1, "\tnew_val %s\n", op->new_val);
        debug(1, 1, 1, "\tob_id: %d\n", op->obj_id);
        debug(1, 1, 1, "\tdisc_name %s\n", op->disc_name);
}

void dump_obj(object * obj)
{
	debug(1, 1, 1, "Object:\n");
	debug(1, 1, 1, "\tid: %d\n", obj->id);
	debug(1, 1, 1, "\tline %s\n", obj->line);
	debug(1, 1, 1, "\tvisible: %d\n", obj->visible);
	debug(1, 1, 1, "\tlog_file_id: %d\n", obj->logfile_id);
	debug(1, 1, 1, "\toffset: %d\n", obj->offset);
}

void dump_disc(int num_objs, object * objs)
{
	int i = 0;
	debug(1,1,1,"Dumping discussion with %d objects\n", num_objs);
	for (i = 0; i < num_objs; i++)
	{
		dump_obj(objs + i);
		debug(1,1,1,"\n");
	}
}

